# frozen_string_literal: true

require 'net/http'

class ArchiveController < APIController
  include Zipline

  def create
    if !params[:urls] || !params[:urls].is_a?(Array)
      raise ArgumentError, "expected format { urls: [ { url: '', filename: '' }, ... ] }"
    end

    images = params[:urls].each_with_object([]) do |element, result|
      result << [element['url'], element['filename']]
    end

    zipline(images, 'archive.zip')
  end

  def show
    raise ArgumentError, "expected HTTP POST"
  end

  def index
    render json: { note: 'Zip URL Combiner/Archiver', version: '0.0.1a' }
  end
end
