# frozen_string_literal: true

class APIController < ActionController::API
  rescue_from ArgumentError, with: :handle_errors_json

private

  def handle_errors_json(err)
    render json: { error: err.class.name, message: err.message }, status: :bad_request
  end
end
