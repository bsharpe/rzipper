# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ArchiveController, type: :request do
  def object_returned
    JSON.parse(response.body, object_class: OpenStruct)
  end

  it 'should return info' do
    get root_path
    expect(response).to have_http_status(:ok)
  end

  it 'should return error if called with a GET' do
    get process_path
    expect(response).to have_http_status(:bad_request)
  end

  it 'should return a zip archive' do
    post process_path, params: {
      urls: [
        {
          url: 'https://media.giphy.com/media/3oz8xD0xvAJ5FCk7Di/giphy.gif',
          filename: 'pic001.gif'
        }
      ]
    }
    expect(response.headers['Content-type']).to eq('application/zip')
    expect(response.headers['Content-Disposition']).to include('attachment')
  end

  context 'bad input should fail' do
    it 'NO PARAMS' do
      post process_path, params: {}

      expect(response).to have_http_status(:bad_request)
      expect(object_returned.error).to eq('ArgumentError')
    end

    it 'NO URLS' do
      post process_path, params: { urls: nil }

      expect(response).to have_http_status(:bad_request)
      expect(object_returned.error).to eq('ArgumentError')
    end

    it 'URLS WRONG TYPE' do
      post process_path, params: { urls: {} }

      expect(response).to have_http_status(:bad_request)
      expect(object_returned.error).to eq('ArgumentError')
    end
  end
end
