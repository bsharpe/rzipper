Rails.application.routes.draw do
  post '/process' => 'archive#create'
  get '/process' => 'archive#show'

  root 'archive#index'
end

